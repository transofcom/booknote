from django.db import models

# Create your models here.

# ==========================================================================
# DB INITIATOR
# ==========================================================================
def init_sex():
    if Sex.objects.all():
        male_model, mail_existed = Sex.objects.get_create(name='Male')
        female_model, femail_existed = Sex.objects.get_create(name='Female')

def init_nationality():
    nationalities = ['Korea', 'America', 'Japan',]
    for each_name in nationalities:
        nationality_model, nationality_existed = Nationality.objects.get_or_create(name=each_name)


def init_tag():
    TAGS = [
        'Development', 'Python', 'Django', 'Agile', 'Scrum', 'OKR', 'Javascript', 'C#', 'C++', 'Ruby', 'Go-lang', 'Java', 'HTML', 'CSS', 'Ajax', 'React', 'Linux', 'CMD', 'Terminal', 'OS', 
        'English', 'Japanese', 'Cantonese', 'Spanish', 'Italian', 'French', 'Portuguese'
        'Fiction', 'Haruki Murakami', 'Keigo Higashino', 'Poul Anderson', 'Kaori Ekuni', 
        'Life Management', 'Time Management', 'Raltionships Management',
        'Philosophy', 'Art', 'Science',
        'Painting', 'Vincent Van Gogh', 'Paul Gauguin', 'Picaso', 'Matisse', 'Frida Kahlo', 'Botticelli', 'Leonardo Da Vinci', 'Michelangelo', 'Paul Cezanne', 'Salvador Dali',
        'Movies', 'Music', 'Sports', 
        ]

    for each_name in TAGS:
        tag_model, tag_existed = Tag.objects.get_or_create(name=each_name)

def init_currency():
    currenies = ['USD', 'KRW', 'JYP', 'RMP', 'HKD', 'AUD']
    for each_name in currenies:
        currency_model, currency_existed = Currency.objects.get_or_create(name=each_name)

def init_cover_type():
    cover_types = ['Paperback', 'Hardcover Case Wrap', 'Hardcover Dust Jacket',]
    for each_name in cover_types:
        cover_model, cover_existed = CoverType.objects.get_or_create(name=each_name)

def init_purchase_reason():
    reasons_eng = {
        'Author': ['Favorite Author', 'Attractive Author', 'Want to know about Author',],
        'Glimpsed': ['tried in a bookstore and attracted sentences',],
        'Recommended': ['Friends/Family/Colleague Recommended', 'Teacher/School Recommended', 'Other Recommneded',],
        'Advertisement': ['TV Advertisement', 'Web Advertisement', 'Radio Advertisement', 'Other Advertisement',],
        'Design': ['cover design', 'layout',],
        'Price': ['cheap', 'reasonable', ],
        'Other': ['no reason', 'co-incedence', ],
        }
    for reason in reasons_eng.items():
        category_name = reason[0]
        purchase_category_model, category_in_db = PurchaseCategory.objects.get_or_create(name_eng=category_name)
        reasons = reason[1]
        for reason_name in reasons:
            pr_model, reason_in_db = PurchaseReason.objects.get_or_create(name_eng=reason_name, category=purchase_category_model)


def init_job():
    jobs = ['Writer', 'Supervisor', 'Translatr', 'Others',]
    for each_name in jobs:
        job_model, job_existed = Job.objects.get_or_create(name_original=each_name)


def init_author():
    pass

def init_book():
    pass


# ==========================================================================
# META
# ==========================================================================
class Nationality(models.Model):
    name = models.CharField(max_length=32)
    img_src = models.CharField(max_length=64, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Nationalities"

    def __str__(self):
        return self.name


class Sex(models.Model):
    name = models.CharField(max_length=6)

    class Meta:
        verbose_name_plural = "Sex"

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=24)

    class Meta:
        verbose_name_plural = "Tag"

    def __str__(self):
        return self.name


class Currency(models.Model):
    name = models.CharField(max_length=6)

    class Meta:
        verbose_name_plural = "Currencies"

    def __str__(self):
        return self.name

class CoverType(models.Model):
    name = models.CharField(max_length=36)

    class Meta:
        verbose_name_plural = "Cover Types"

    def __str__(self):
        return self.name

class PurchaseCategory(models.Model):
    name_eng = models.CharField(max_length=36)

    class Meta:
        verbose_name_plural = "Categories of REASONS OF PURCHASE"

    def __str__(self):
        return self.name


class PurchaseReason(models.Model):
    name_eng = models.TextField()
    name_kor = models.TextField(blank=True, null=True)
    category = models.ForeignKey("PurchaseCategory", on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name_plural = "REASONS OF PURCHASE"

    def __str__(self):
        return self.name

# ==========================================================================
# Booknote
# ==========================================================================
class Publisher(models.Model):
    name_original = models.CharField(max_length=32, blank=True, null=True)
    name_localized = models.CharField(max_length=16, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Publishers"

    def __str__(self):
        if self.name_original:
            return self.name_original
        elif self.name_localized:
            return self.name_localized
        return "Publisher #{}".format(self.id)

    def number_books(self):
        return len(self.book_set.objects.all())

class Author(models.Model):
    name_eng = models.CharField(max_length=32, blank=True, null=True)
    name_kor = models.CharField(max_length=16, blank=True, null=True)
    nationality = models.ForeignKey('Nationality', on_delete=models.CASCADE, blank=True, null=True)
    sex = models.ForeignKey('Sex', on_delete=models.CASCADE, blank=True, null=True)
    date_birth = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Authors"

    def __str__(self):
        if self.name_eng:
            return self.name_eng
        elif self.name_kor:
            return self.name_kor
        return "AUTHOR #{}".format(self.id)

    def number_books(self):
        return len(self.book_set.objects.all())


class People(models.Model):
    name_original = models.CharField(max_length=32, blank=True, null=True)
    name_localized = models.CharField(max_length=16, blank=True, null=True)
    nationality = models.ForeignKey('Nationality', on_delete=models.CASCADE, blank=True, null=True)
    jobs = models.ManyToManyField('Job', blank=True)

    class Meta:
        verbose_name_plural = "People"

    def __str__(self):
        if self.name_original:
            return self.name_original
        elif self.name_localized:
            return self.name_localized
        return "People #{}".format(self.id)

    def number_books(self):
        return len(self.book_set.objects.all())


class Job(models.Model):
    name_original = models.CharField(max_length=32, blank=True, null=True)
    name_localized = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Jobs"

    def __str__(self):
        if self.name_original:
            return self.name_original
        elif self.name_localized:
            return self.name_localized
        return "People #{}".format(self.id)


class Book(models.Model):
    # THIS IS ORIGINAL BOOK, BASIC INFO
    title_original = models.CharField(max_length=128)
    authors = models.ManyToManyField('Author')
    date_release = models.DateField(blank=True, null=True)

    tags = models.ManyToManyField('Tag', blank=True)

    class Meta:
        verbose_name_plural = "Books"

    def __str__(self):
        if title_eng:
            return self.title_eng
        elif title_kor:
            return self.title_kor
        return "BOOK #{}".format(self.id)


class BookVersion(models.Model):
    book = models.ForeignKey('Book', on_delete=models.CASCADE)
    title_version = models.CharField(max_length=128)
    isbn = models.CharField(max_length=13, blank=True, null=True)
    publisher = models.ManyToManyField('Publisher', blank=True, related_name='publisher+')
    translators = models.ManyToManyField('People', blank=True, related_name='translators+')
    supervisors = models.ManyToManyField('People', blank=True, related_name='supervisors+')

    price_original = models.IntegerField(default=0)
    price_original_currency = models.ForeignKey('Currency', on_delete=models.CASCADE, blank=True, null=True)

    cover_type = models.ForeignKey('CoverType', on_delete=models.CASCADE, blank=True, null=True)

    date_published = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Book Versions"

    def __str__(self):
        return self.title_version

    def get_isbn(self):
        if self.isbn.__contains__('978') or self.isbn.__contains__('979'):
            # THIS IS ISBN 13 DIGITS
            prefix = self.isbn[:3]
            reg_group = self.isbn[3:5]
            publisher = self.isbn[5:9]
            title = self.isbn[9:12]
            check_digit = self.isbn[12]
            return "{}-{}-{}-{}-{}".format(prefix, reg_group, publisher, title, check_digit)

        else:
            # THIS IS ISBN 10 DIGITS
            return self.isbn


class CustomBook(models.Model):
    book_version = models.ForeignKey('BookVersion', on_delete=models.CASCADE)

    price_purchased = models.IntegerField(default=0)
    # let's consider custom_book uses same currency as book_version
    # price_purchased_currency = models.ForeignKey('Currency', on_delete=models.CASCADE, blank=True, null=True)

    reason_of_purchase = models.ForeignKey('PurchaseReason', on_delete=models.CASCADE, blank=True, null=True)

    date_purchased = models.DateField(blank=True, null=True)
    date_first_read = models.DateField(blank=True, null=True)

    # Comments means final comments of mine
    comments = models.TextField(blank=True, null=True)

    # Notes means individual note while reading, can specify page no.
    notes = models.ManyToManyField('Note', blank=True)


class Note(models.Model):
    note = models.TextField(blank=True, null=True)
    date_add = models.DateField(auto_now_add=True)
    page = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = "Notes"

    def __str__(self):
        if self.remark:
            return self.remark
        return "REMARK #{}".format(self.id)



