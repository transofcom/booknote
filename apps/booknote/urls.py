# apps/app1/urls.py

from django.conf.urls import url
from . import views
app_name = 'booknote'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'^download$', views.download, name='download'),

    #url(r'^home$', views.home, name='home'),
    #url(r'^mail$', views.mail, name='mail'),
    #url(r'^events$', views.events, name='events'),
    #url(r'^contacts$', views.contacts, name='contacts'),

    #url(r'^gettoken/$', views.gettoken, name='gettoken'),
]