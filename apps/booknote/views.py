from django.shortcuts import render

# Create your views here.
from .models import Author, Book, Note

def index(request):
    context = {
        "all_books": Book.objects.all()
    }
    return render(request, 'booknote/index.html', context)
