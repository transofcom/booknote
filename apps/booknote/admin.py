from django.contrib import admin

# Register your models here.
from apps.booknote.models import *


# ==========================================================================
# META
# ==========================================================================
admin.site.register(Nationality)
admin.site.register(Sex)
admin.site.register(Tag)
admin.site.register(Currency)
admin.site.register(CoverType)
admin.site.register(PurchaseCategory)
admin.site.register(PurchaseReason)


# ==========================================================================
# Booknote
# ==========================================================================
admin.site.register(Publisher)
admin.site.register(People)
admin.site.register(Job)
admin.site.register(Book)
admin.site.register(BookVersion)
admin.site.register(CustomBook)
admin.site.register(Note)

