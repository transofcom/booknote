"""
# DEV.UTILS
# 2020.04.23

0 title_eng
1 title_eng
2 authos
3 translater
4 supervisor
5 publisher
6 수량
7 original_price
8 price_my
9 purchased

"""


from apps.booknote.models import *
import datetime

def dict_builder(title, values):
    output = dict()
    for index, info in enumerate(values):
        if len(info.split(',')) > 1:
            seperated_list = info.split(',')
            data_list = []
            for item in seperated_list:
                data_list.append(item.strip())
            info = data_list
        output.__setitem__(title[index], info)
    return output

def date_builder(input_str):
    """
    input_str = '2020-04-20 오전 1:46:25'
    """
    ymd, am_pm, hours = input_str.split(" ")
    year, month, day = ymd.split('-')
    hour, minute, second = hours.split(':')
    if am_pm == '오후':
        hour = 12 + int(hour)
    return datetime.datetime(int(year), int(month), int(day), int(hour), int(minute), int(second))

def create_book(values):
    pass
